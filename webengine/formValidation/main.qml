import QtQuick 2.0
import QtQuick.Window 2.0
import QtWebEngine 1.0

Window {
    visible: true
    width: 1024
    height: 860
    title: "BioinforCloud Helper"

    WebEngineView {
        anchors.fill: parent
        url: "http://www.bioinforcloud.com:5556/Home"
    }
}
